#ifndef SEND_H
#define SEND_H

#include <QObject>
#include <QUdpSocket>
#include <QObject>
#include <QJsonArray>
#include <QJsonObject>

class Send : public QObject
{
    Q_OBJECT
public:
    explicit Send(QObject *parent = nullptr);

signals:


public slots:

    void Sender();

    void read();

private:

    QUdpSocket *socket;

    int bordernum_ ;

    double num_[10] ;


};

#endif // SEND_H
