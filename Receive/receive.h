#ifndef RECEIVE_H
#define RECEIVE_H

#include <QObject>
#include <QJsonArray>
#include <QJsonObject>
#include <QUdpSocket>

class Receive : public QObject
{
    Q_OBJECT
public:
    explicit Receive(QObject *parent = nullptr);


signals:

public slots:

    void sender();
    void write(int avg);

private:

    QUdpSocket *socket;


};

#endif // RECEIVE_H
