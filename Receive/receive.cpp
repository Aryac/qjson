#include "receive.h"
#include <QCoreApplication>
#include <QUdpSocket>
#include <QtNetwork>
#include <iostream>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonValue>
#include <string>
#include <QString>
using namespace std ;
Receive::Receive(QObject *parent) : QObject(parent)

{

    socket = new QUdpSocket(this);

    socket->bind( QHostAddress::LocalHost, 1376) ;

    connect(socket, SIGNAL(readyRead()), this, SLOT(sender()));

}

void Receive ::sender()
{
    QByteArray data1 ;

    data1.resize(socket->pendingDatagramSize());

    QHostAddress sender ;

    quint16 senderport ;

    socket->readDatagram(data1.data(), data1.size(), &sender, &senderport);

    qDebug(data1);

    QString str = data1;

    QByteArray bytearray = str.toUtf8();

    QString backToString = str;

    qDebug() << str << endl;

    QJsonDocument jsondOc = QJsonDocument::fromJson(str.toUtf8());;

    QJsonObject jsonObject = jsondOc.object();

    QJsonArray jsonArr = jsonObject["Numbers"].toArray();

    double sum = 0;

    for (int i = 0; i < jsonArr.count(); i++) {

        sum += jsonArr.at(i).toInt();
    }

    int avg = sum / jsonArr.count();

    write(avg);
}

void Receive::write(int avg)
{

    QByteArray datagram ;

    QJsonObject object;

    object["Avg"]= avg;

    QJsonDocument doc(object);

    datagram=doc.toJson();

    QString str = datagram;

    qDebug() << str << endl;

    socket->writeDatagram(datagram ,QHostAddress::LocalHost , 1234) ;
}

































